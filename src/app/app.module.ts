
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SignaturePadModule } from 'angular2-signaturepad';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpClientModule } from '@angular/common/http';
import { Base64ToGallery } from '@ionic-native/base64-to-gallery';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Base64 } from '@ionic-native/base64';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { BrowserTab } from '@ionic-native/browser-tab';
import { ThemeableBrowser } from '@ionic-native/themeable-browser';
import { Printer } from '@ionic-native/printer';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { CurrencyPipe } from '@angular/common';
import { MyApp } from './app.component';
import { IonicStorageModule } from '@ionic/storage';
import { NativeStorage } from '@ionic-native/native-storage';
import { Geolocation } from '@ionic-native/geolocation';
import { Network } from '@ionic-native/network';
import { OpenNativeSettings } from '@ionic-native/open-native-settings';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { UniqueDeviceID } from '@ionic-native/unique-device-id';
import {Camera} from '@ionic-native/camera';
import { FilePath } from '@ionic-native/file-path';
import { ProgressBarComponent } from '../components/progress-bar/progress-bar';




@NgModule({
  declarations: [
    MyApp,
    ProgressBarComponent
  ],
  imports: [
    BrowserModule,
    SignaturePadModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp, {
      scrollPadding: false,
      scrollAssist: false,
      autoFocusAssist: false,
    }),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Base64ToGallery,
    AndroidPermissions,
    Base64,
    FileTransfer,
    File,
    InAppBrowser,
    BrowserTab,
    ThemeableBrowser,
    Printer,
    ScreenOrientation,
    CurrencyPipe,
    NativeStorage,
    Geolocation,
    Network,
    OpenNativeSettings,
    LocationAccuracy,
    UniqueDeviceID,
    Camera,
    FilePath,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
