
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  
  rootPage: any = 'LoginPage';

  pages: Array<{ title: string, component: any }>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'การบ้าน', component: 'HomePage' },
      { title: 'ข่าวสาร', component: 'NewsPage' },
      { title: 'O - Net', component: 'HomePage' },
      { title: 'สถานะ มส.', component: 'HomePage' },
      { title: 'คะแนนเก็บ/คะแนนสอบ', component: 'HomePage' },
      { title: 'ตารางเรียน/สอบ', component: 'HomePage' },
      { title: 'ใบคำศัพท์', component: 'VocabularyPage' },
      { title: 'LOG OUT', component: 'LoginPage' }
    ];

    window.addEventListener("keyboardDidShow", () => {
      document.activeElement.scrollIntoView(false);

      const elem: HTMLCollectionOf<Element> = document.getElementsByClassName("scroll-content");
      if (elem !== undefined && elem.length > 0) {
        elem[elem.length - 1].scrollTop += 40;
      }

    });

  }


  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page: any): void {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario

      this.nav.push(page.component);
    
  }

}
