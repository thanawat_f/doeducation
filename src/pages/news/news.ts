import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Platform } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-news',
  templateUrl: 'news.html',
})
export class NewsPage {
  height: any;
  width: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public platform: Platform) {
    this.width = '100%';
    platform.ready().then((readySource) => {
      this.height = (platform.height() * 70) /100;
      this.height = platform.height() - this.height;
      this.height = this.height+'px';
    });
  }

  ionViewDidLoad() {
    
  }

}
