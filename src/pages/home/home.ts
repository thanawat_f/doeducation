import { Component } from '@angular/core';
import { Platform,NavController,AlertController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { OpenNativeSettings } from '@ionic-native/open-native-settings';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { IonicPage } from 'ionic-angular';
import { NativeStorage } from '@ionic-native/native-storage';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  constructor(public navCtrl: NavController,private geolocation: Geolocation, private alertCtrl: AlertController,private openNativeSettings: OpenNativeSettings,private locationAccuracy: LocationAccuracy,public platform: Platform,private nativeStorage: NativeStorage) {

  }

  profile(){
    this.navCtrl.push('ProfilePage');
  }

  news(){
    this.navCtrl.push('NewsPage');
  }

  vocabulary(){
    this.navCtrl.push('VocabularyPage');
  }

}
