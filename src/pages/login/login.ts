import { Component } from '@angular/core';
import { IonicPage, NavController, Platform, NavParams, AlertController, LoadingController, MenuController  } from 'ionic-angular';
import { NativeStorage } from '@ionic-native/native-storage';
import { HttpClient } from '@angular/common/http';
import { UniqueDeviceID } from '@ionic-native/unique-device-id';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  constructor(public navCtrl: NavController,private menu: MenuController,public platform: Platform, public navParams: NavParams, private nativeStorage: NativeStorage, private alertCtrl: AlertController, public loadingCtrl: LoadingController, public http: HttpClient, private uniqueDeviceID: UniqueDeviceID) {
    platform.registerBackButtonAction(() => {
      let view = this.navCtrl.getActive();
      if (view.component.name == "HomePage") {
      }else if(view.component.name == "LoginPage"){
        this.platform.exitApp();
      }else{
        this.navCtrl.pop({});
      }
    });
  }

  ionViewDidEnter() {
    this.menu.swipeEnable(false);
  }

  login(){
    this.menu.swipeEnable(true);
    this.navCtrl.setRoot('HomePage');
  }

  change_password(){
    this.navCtrl.push('ChangePasswordPage');
  }

}
